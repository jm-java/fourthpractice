package joraly;

import java.util.Objects;

public class Order {

    String brand;
    String model;
    Integer quantity;

    public Order(String brand, String model, Integer quantity) {
        this.brand = brand;
        this.model = model;
        this.quantity = quantity;
        Main.orders.add(this);
    }

    @Override
    public String toString() {
        return "\nТовар: \n" +
                "Бренд=" + brand + '\n' +
                "Модель=" + model + '\n' +
                "Количество=" + quantity;
    }
}
