package joraly;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static String[] brands = {"Casio", "Omega", "Tissot"};
    public static String[][] models = {
            {"GShock", "ProTrek", "Baby-G"},
            {"Constellation", "Seamaster", "Speedmaster"},
            {"PRX", "PR100", "Chrono"}
    };
    public static List<User> users = new ArrayList<User>();
    public static List<Order> orders = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Действия:\n" +
                    "1. Сделать заказ\n" +
                    "2. Посмотреть все заказы\n" +
                    "0. Выйти");

            int x = sc.nextInt();
            sc.nextLine();

            switch (x) {
                case 1 -> {
                    System.out.println("Введите ваше ФИО");
                    String name = sc.nextLine();

                    System.out.println("Введите вашу электронную почту");
                    String email = sc.nextLine();

                    System.out.println("Введите ваш номер телефона");
                    String phoneNumber = sc.nextLine();

                    System.out.println("Введите количество товаров, что вы хотите купить");
                    int orderCount = sc.nextInt();

                    List<Order> orders1 = new ArrayList<>();
                    for (int z = 0; z < orderCount; z++) {

                        for (int i = 0; i < models.length; i++) {
                            System.out.print(i+1 + ". "+brands[i] + " | " + models[i][0] + "  ");
                            for (int j = 1; j < models[i].length; j++) {
                                System.out.print(models[i][j] + "  ");
                            }
                            System.out.println();
                        }
                            System.out.println("Введите номер товара");
                        int nb = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Введите количество товара");
                        int qt = sc.nextInt();
                        sc.nextLine();

                        Order order = new Order(brands[nb / models[0].length], models[nb % models.length][nb / models[0].length], qt);
                    }

                    User user = new User(name, email, phoneNumber, orders);
                }
                case 2 -> {
                    List<User> users1 = users.stream().distinct().collect(Collectors.toList());
                    for (var user : users1) {
                        System.out.println(user);
                    }
                }
                case 3 -> {
                    System.exit(0);
                }
            }
        }

    }
}