package joraly;

import java.util.List;
public class User {
    String name;
    String email;
    String phoneNumber;
    List<Order> orders;

    public User(String name, String email, String phoneNumber, List<Order> orders) {
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.orders = orders;
        Main.users.add(this);
    }

    @Override
    public String toString() {
        return "Пользователь: " +
                "Имя=" + name + '\n' +
                "Почта=" + email + '\n' +
                "Номер телефона=" + phoneNumber + '\n' +
                "Заказы=" + orders;
    }
}
